const route = require("express").Router();
const {users_control,biodatacontrol} = require("../controller");

route.post("/usergame", users_control.createuser);
route.get("/usergame", users_control.getuser)
// route.get("/usergame/:id", user_game_route.getuser_gameid)
// route.post("/usergame/:id", user_game_route.edituser_game)
// route.get("/edit/usergame/:id",user_game_route.edituser_gameview)
// route.get("/delete/usergame/:id", user_game_route.deleteuser_game)
// route.get("/add/usergame", user_game_route.create_view)


route.post("/biodata", biodatacontrol.createbiodata);
route.get("/biodata", biodatacontrol.getbiodata);

//
// route.post("/history", historycontroller.createhistory);
// route.get("/history", historycontroller.gethistory);
//
// route.get("/", user_game_route.loginpage)
// route.post("/login", user_game_route.loginmethod)
module.exports = route;
