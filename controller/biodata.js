const { user_biodata , users} = require("../models")

class biodatacontrol {
  static async createbiodata(req, res, next) {
    try {
      const { userid,firstname, lastname } = req.body
      const data = await user_biodata.create({
      userid,firstname, lastname
      })
      res.status(201).json(data)
    } catch (error) {
console.log(error)
    }
  }

  static async getbiodata(req, res, next) {
    try {
      const data = await user_biodata.findAll({
        include: [
          {
            model: users
          }
        ]
      })
      // let dataarr = data.map(x=> x.dataValues)
      // res.render("biodata",{data : dataarr});
      res.status(200).json({data})
    } catch (error) {
      console.log(error)
    }
  }
}

module.exports = biodatacontrol;
