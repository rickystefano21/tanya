const users_control = require("./users");
const biodatacontrol = require("./biodata");

module.exports = {
    users_control,
    biodatacontrol
};
